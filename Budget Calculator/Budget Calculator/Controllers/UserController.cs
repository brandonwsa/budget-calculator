﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Budget_Calculator.Models;

namespace Budget_Calculator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private IMongoCollection<User> _userCollection;

        /// <summary>
        /// Constructor takes mongo client, uses the client to connect to the database and gets the user collection.
        /// </summary>
        /// <param name="client"></param>
        public UserController(IMongoClient client)
        {
            var database = client.GetDatabase("budget-calculator-db");
            _userCollection = database.GetCollection<User>("users");
        }
        
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return _userCollection.Find(u => true).ToList(); //u => u.Name == "joe"
        }

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Error Message if ran into error, or Ok status 200 if success.</returns>
        [HttpPost("create")]
        public IActionResult Create([FromBody]User user)
        {
            try
            {
                if (user == null || !ModelState.IsValid)
                {
                    return BadRequest(ErrorCode.UserNameEmailPasswordRequired.ToString());
                }

                //check if user name and email exists in database already.
                //grabbing the document instead of using .Any<User>() to capture a bool value if user exists allows us to not have to ping database multiple times to 
                //get accurate information on whether or not the user provided name and/or email already exists.
                //Was using, bool userNameExists = _userCollection.Find<User>(u => u.Name == user.Name).Any<User>();
                //and, bool userEmailExists = _userCollection.Find<User>(u => u.email == user.email).Any<User>();
                //which required 2 database calls instead of one.
                try
                {
                    var db_user = _userCollection.Find<User>(u => u.Name == user.Name || u.Email == user.Email).Single<User>();

                    if (db_user.Email == user.Email && db_user.Name == user.Name)
                    {
                        return StatusCode(StatusCodes.Status409Conflict, ErrorCode.UserNameAndEmailAlreadyInUse.ToString());
                    }
                    else if (db_user.Email == user.Email)
                    {
                        return StatusCode(StatusCodes.Status409Conflict, ErrorCode.UserEmailAlreadyInUse.ToString());
                    }
                    else if (db_user.Name == user.Name)
                    {
                        return StatusCode(StatusCodes.Status409Conflict, ErrorCode.UserNameAlreadyInUse.ToString());
                    }

                }
                catch (Exception)
                {
                    //do nothing, means user doesnt exists which is good!
                }

                //bool userEmailExists = _userCollection.Find<User>(u => u.Email == user.Email).Any<User>();
                //if (userEmailExists)
                //{
                //    return StatusCode(StatusCodes.Status409Conflict, ErrorCode.UserEmailAlreadyInUse.ToString());
                //}

                _userCollection.InsertOne(user);
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateUser.ToString());
            }

            return Ok("User "+user.Name+" created!");
        }

        /// <summary>
        /// Updates the specific user from the Id in the url along with the passed in new user from the body.
        /// Requires the Id to be passed in with user object in the body as well.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns>NotFound if not found, BadRequest if couldnt update user, and NoContent when successfully updating the user.</returns>
        [HttpPut("update/{id}")]
        public IActionResult Update(string id, [FromBody] User user)
        {
            try
            {
                if (user == null)
                {
                    return BadRequest(ErrorCode.UserNameEmailPasswordRequired.ToString());
                }

                var existingUser = _userCollection.Find<User>(id);

                if (existingUser == null)
                {
                    return NotFound(ErrorCode.UserNotFound.ToString());
                }

                //make the updater and update the user.
                var update = Builders<User>.Update.Set("Name", user.Name)
                                                  .Set("Email", user.Email)
                                                  .Set("Password", user.Password);

                _userCollection.UpdateOne<User>(u => u.Id == id, update);
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotUpdateUser.ToString());
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a user based on the provided user Id in the url.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>NotFound if not found, BadRequest if couldnt delete user, and NoContent when successfully deleting the user.</returns>
        [HttpDelete("delete/{id}")]
        public IActionResult Delete(string id)
        {
            try
            {
                var user = _userCollection.FindOneAndDelete<User>(u => u.Id == id);

                if (user == null)
                {
                    return NotFound(ErrorCode.UserNotFound.ToString());
                }
            }
            catch(Exception)
            {
                return BadRequest(ErrorCode.CouldNotDeleteUser.ToString());
            }

            return NoContent();
        }

    }
}
