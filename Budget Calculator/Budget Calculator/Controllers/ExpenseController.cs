﻿using Budget_Calculator.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExpenseController : Controller
    {
        private IMongoCollection<Expense> _expenseCollection;

        public ExpenseController(IMongoClient client)
        {
            var database = client.GetDatabase("budget-calculator-db");
            _expenseCollection = database.GetCollection<Expense>("expenses");
        }

        /// <summary>
        /// Get all of the expenses.
        /// </summary>
        /// <returns>A list of the expenses.</returns>
        [HttpGet]
        public IEnumerable<Expense> Get()
        {
            return _expenseCollection.Find(e => true).ToList();
        }

        /// <summary>
        /// Create an expense for a user from the expense passed in from the body.
        /// A user can only have one expense.
        /// </summary>
        /// <param name="expense"></param>
        /// <returns>Ok if created successfully. BadRequest if not a valid expense or couldnt create expense. Status409 if expense exists already. BadRequest if numbers are not positive or zero.</returns>
        [HttpPost("create")]
        public IActionResult Create([FromBody] Expense expense)
        {
            try
            {
                if (expense == null)
                {
                    return BadRequest(ErrorCode.ExpenseUserMonthlyOrYearlyRequired.ToString());
                }

                //check if user entered any expenses
                if (expense.EnteredMonthly == false && expense.EnteredYearly == false)
                {
                    return BadRequest(ErrorCode.ExpenseNotValid.ToString());
                }

                //check if user already has an expense made
                bool expenseExists = _expenseCollection.Find<Expense>(e => e.User == expense.User).Any<Expense>();
                if (expenseExists)
                {
                    return StatusCode(StatusCodes.Status409Conflict, ErrorCode.ExpenseAlreadyExistsForUser.ToString());
                }

                //check if user entered monthly or yearly expense and calc the missing one.
                if (expense.EnteredMonthly)
                {
                    //check if user entered valid numbers >= 0
                    bool validMonthly = expense.Monthly.enteredValidReport();

                    if (validMonthly)
                    {
                        Utility.Utilities.calcYearlyExpense(expense);
                    }
                    else
                    {
                        return BadRequest(ErrorCode.ExpenseContainsNegativeNumbers.ToString());
                    }
                    
                }
                else if (expense.EnteredYearly)
                {
                    //check if user entered valid numbers >= 0
                    bool validYearly = expense.Yearly.enteredValidReport();

                    if (validYearly)
                    {
                        Utility.Utilities.calcMonthlyExpense(expense);
                    }
                    else
                    {
                        return BadRequest(ErrorCode.ExpenseContainsNegativeNumbers.ToString());
                    }
                    
                }

                _expenseCollection.InsertOne(expense);

            }
            catch (Exception)
            {
                BadRequest(ErrorCode.CouldNotCreateExpense.ToString());
            }


            return Ok("Expense created!");
        }

        /// <summary>
        /// Updates the expense passed in from the body.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="expense"></param>
        /// <returns>NoContent if expense updated successfully. BadRequest if no expense passed in, didnt enter monthly or yearly, expense contains negatives, or could not update 
        /// expense. NotFound if expense does not exists in the database.</returns>
        [HttpPut("update/{id}")]
        public IActionResult Update(string id, [FromBody] Expense expense)
        {
            try
            {
                if (expense == null)
                {
                    return BadRequest(ErrorCode.ExpenseUserMonthlyOrYearlyRequired.ToString());
                }

                //check if expense exists in database.
                try
                {
                    var existingExpense = _expenseCollection.Find<Expense>(e => e.Id == id).Single<Expense>(); //returns exception if not found.
                }
                catch (Exception)
                {
                    return NotFound(ErrorCode.ExpenseNotFound.ToString());
                }

                //check if user entered any expenses
                if (expense.EnteredMonthly == false && expense.EnteredYearly == false)
                {
                    return BadRequest(ErrorCode.ExpenseNotValid.ToString());
                }

                //check if user entered monthly or yearly expense and calc the missing one.
                if (expense.EnteredMonthly)
                {
                    //check if user entered valid numbers >= 0
                    bool validMonthly = expense.Monthly.enteredValidReport();

                    if (validMonthly)
                    {
                        Utility.Utilities.calcYearlyExpense(expense);
                    }
                    else
                    {
                        return BadRequest(ErrorCode.ExpenseContainsNegativeNumbers.ToString());
                    }

                }
                else if (expense.EnteredYearly)
                {
                    //check if user entered valid numbers >= 0
                    bool validYearly = expense.Yearly.enteredValidReport();

                    if (validYearly)
                    {
                        Utility.Utilities.calcMonthlyExpense(expense);
                    }
                    else
                    {
                        return BadRequest(ErrorCode.ExpenseContainsNegativeNumbers.ToString());
                    }

                }

                //make the update
                var update = Builders<Expense>.Update.Set("monthly", expense.Monthly)
                                                     .Set("yearly", expense.Yearly)
                                                     .Set("entered_monthly", expense.EnteredMonthly)
                                                     .Set("entered_yearly", expense.EnteredYearly);

                _expenseCollection.FindOneAndUpdate<Expense>(e => e.Id == id, update);

            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotUpdateExpense.ToString());
            }


            return NoContent();
        }

        /// <summary>
        /// Deletes an expense based on the expense id from the url.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>NoContent if deleted successfully. NotFound if no expense matches the id. BadRequest if couldnt delete expense.</returns>
        [HttpDelete("delete/{id}")]
        public IActionResult Delete(string id)
        {
            try
            {
                var expense = _expenseCollection.FindOneAndDelete<Expense>(e => e.Id == id);

                if (expense == null)
                {
                    return NotFound(ErrorCode.ExpenseNotFound.ToString());
                }
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotDeleteExpense.ToString());
            }


            return NoContent();
        }
    }
}
