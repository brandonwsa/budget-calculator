﻿using Budget_Calculator.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class IncomeController : Controller
    {
        private IMongoCollection<Income> _incomeCollection;

        /*
         * Constructor takes mongo client, uses the client to connect to the database and gets the income collection.
         */
        public IncomeController(IMongoClient client)
        {
            var database = client.GetDatabase("budget-calculator-db");
            _incomeCollection = database.GetCollection<Income>("income");
        }

        [HttpGet]
        public IEnumerable<Income> Get()
        {
            return _incomeCollection.Find(i => true).ToList();
        }

        /// <summary>
        /// Create a new income for a user. A user can have multiple incomes. income object needs to have user, name, and either a yearly or monthly income that is not 0.
        /// </summary>
        /// <param name="income"></param>
        /// <returns>Ok if created successfully or BadRequest with an error code if not.</returns>
        [HttpPost("create")]
        public IActionResult Create([FromBody] Income income)
        {
            try
            {
                if (income == null || !ModelState.IsValid)
                {
                    return BadRequest(ErrorCode.IncomeUserAndNameRequired.ToString());
                }

                //check if a positive income amount is set
                if (income.Yearly.income < 0 || income.Monthly.income < 0)
                {
                    return BadRequest(ErrorCode.IncomeNotValid.ToString());
                }

                //check if positive retirement is set.
                if (income.Monthly.retirement < 0 || income.Yearly.retirement < 0)
                {
                    return BadRequest(ErrorCode.IncomeRetirementNotValid.ToString());
                }

                //check if positive savings is set.
                if (income.Monthly.savings < 0 || income.Yearly.savings < 0)
                {
                    return BadRequest(ErrorCode.IncomeSavingsNotValid.ToString());
                }

                //check if retirement and savings exceed the income amount.
                if (income.Yearly.income < income.Yearly.retirement + income.Yearly.savings || income.Monthly.income < income.Monthly.retirement + income.Monthly.savings)
                {
                    return BadRequest(ErrorCode.IncomeLessThanRetirementPlusSavings.ToString());
                }

                //check if income name exists for user already
                var incomeExists = _incomeCollection.Find<Income>(i => i.User == income.User && i.Name == income.Name).Any<Income>();
                if (incomeExists)
                {
                    return StatusCode(StatusCodes.Status409Conflict, ErrorCode.IncomeNameAlreadyInUseForThisUser.ToString());
                }

                //check if user provided their yearly or monthly income or neither.
                //For the frontend, if user selects yearly, make the monthly object with everything equal to 0. Vice versa
                if (income.Yearly.income == 0 && income.Monthly.income == 0)
                {
                    return BadRequest(ErrorCode.IncomeMonthlyOrYearlyRequired.ToString());
                }
                else if (income.Yearly.income == 0)
                {
                    income = Utility.Utilities.calcIncomeFromMonthly(income);
                }
                else if (income.Monthly.income == 0)
                {
                    income = Utility.Utilities.calcIncomeFromYearly(income);
                }

                //check if savings is less than income after taxes.
                if (income.Yearly.income_after_retirement_and_taxes < income.Yearly.savings || income.Monthly.income_after_retirement_and_taxes < income.Monthly.savings)
                {
                    return BadRequest(ErrorCode.IncomeAfterTaxesLessThanSavings.ToString());
                }

                _incomeCollection.InsertOne(income);
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotCreateIncome.ToString());
            }

            return Ok("Income created!");
        }

        /// <summary>
        /// Update the income from the body of the request using the income id from the url.
        /// Users can update either monthly or yearly information (income, retirement, and/or savings), state_income_tax, federal_income_tax, and name.
        /// At the moment, requires the entire income object to be passed in from the body including every field. Can refactor to only change what is passed in.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="income"></param>
        /// <returns>NoContent if updated successfully or BadRequest if failed to update or NotFound if no income exists to update.</returns>
        [HttpPut("update/{id}")]
        public IActionResult Update(string id, [FromBody] Income income)
        {
            try
            {
                if (income == null)
                {
                    return BadRequest(ErrorCode.IncomeUserAndNameRequired.ToString());
                }

                //check if income is appropriate and not less than 0
                if (income.Monthly.income < 0 || income.Yearly.income < 0)
                {
                    return BadRequest(ErrorCode.IncomeNotValid.ToString());
                }

                try
                {
                    //see if income exists in database
                    var existingIncome = _incomeCollection.Find<Income>(i => i.Id == id).Single<Income>(); //returns exception if not found.

                    //see if user updated the monthly or yearly information
                    if (income.Monthly.income != existingIncome.Monthly.income || income.Monthly.retirement != existingIncome.Monthly.retirement || 
                        income.Monthly.savings != existingIncome.Monthly.savings)
                    {
                        income = Utility.Utilities.calcIncomeFromMonthly(income);
                    }
                    else if (income.Yearly.income != existingIncome.Yearly.income || income.Yearly.retirement != existingIncome.Yearly.retirement || 
                        income.Yearly.savings != existingIncome.Yearly.savings)
                    {
                        income = Utility.Utilities.calcIncomeFromYearly(income);
                    }
                    //check if user only updated taxes and calc information based on the change.
                    else if (income.Federal_income_tax != existingIncome.Federal_income_tax || income.State_income_tax != existingIncome.State_income_tax)
                    {
                        income = Utility.Utilities.calcIncomeFromYearly(income);
                    }

                    //make updater and update the income
                    var update = Builders<Income>.Update.Set("name", income.Name)
                                                        .Set("state_income_tax", income.State_income_tax)
                                                        .Set("federal_income_tax", income.Federal_income_tax)
                                                        .Set("yearly", income.Yearly)
                                                        .Set("monthly", income.Monthly);

                    _incomeCollection.UpdateOne<Income>(i => i.Id == id, update);

                }
                catch (Exception)
                {
                    return NotFound(ErrorCode.IncomeNotFound.ToString());
                }
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotUpdateIncome.ToString());
            }

            return NoContent();
        }


        /// <summary>
        /// Deletes the income based on its id in the url.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public IActionResult Delete(string id)
        {
            try
            {
                var income = _incomeCollection.FindOneAndDelete<Income>(i => i.Id == id);

                if (income == null)
                {
                    return NotFound(ErrorCode.IncomeNotFound.ToString());
                }
            }
            catch (Exception)
            {
                return BadRequest(ErrorCode.CouldNotDeleteIncome.ToString());
            }

            return NoContent();
        }
    }
}
