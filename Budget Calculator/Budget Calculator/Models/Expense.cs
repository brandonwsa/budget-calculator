﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Budget_Calculator.Objects;

namespace Budget_Calculator.Models
{
    public class Expense
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required]
        [BsonElement("user")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string User { get; set; }

        [BsonElement("monthly")]
        public ExpenseReport Monthly { get; set; }

        [BsonElement("yearly")]
        public ExpenseReport Yearly { get; set; }

        [BsonElement("entered_monthly")]
        public bool EnteredMonthly { get; set; }

        [BsonElement("entered_yearly")]
        public bool EnteredYearly { get; set; }

    }
}
