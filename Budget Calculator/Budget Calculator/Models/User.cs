﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Models
{
    /*
    [BsonIgnoreExtraElements] //use to flat out ignore extra elements from db without storing them.
    */
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required]
        [BsonElement("name")]
        public string Name { get; set; }

        [Required]
        [BsonElement("email")]
        public string Email { get; set; }

        [Required]
        [BsonElement("password")] //use to specify unique name in c# if you'd like. password here refers to name of element in mongodb.
        public string Password { get; set; } 


        /*
        //use if you have info from db that you dont want to include, but want to still store in an array.
        [BsonExtraElements]
        public object[] Bucket { get; set; }
        */
    }
}
