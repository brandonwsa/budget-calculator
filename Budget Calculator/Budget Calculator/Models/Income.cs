﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Budget_Calculator.Objects;

namespace Budget_Calculator.Models
{
    public class Income
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Required]
        [BsonElement("user")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string User { get; set; }

        [Required]
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("state_income_tax")]
        public double State_income_tax { get; set; }

        [BsonElement("federal_income_tax")]
        public double Federal_income_tax { get; set; }

        [BsonElement("yearly")]
        public IncomeReport Yearly { get; set; }

        [BsonElement("monthly")]
        public IncomeReport Monthly { get; set; }
    }
}
