﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Budget_Calculator.Models;
using Budget_Calculator.Objects;

namespace Budget_Calculator.Utility
{
    public static class Utilities
    {
        /// <summary>
        /// Calculates income based on a change in the yearly information for the income.
        /// </summary>
        /// <param name="income"></param>
        /// <returns>The new income</returns>
        public static Income calcIncomeFromYearly(Income income)
        {
            //calc yearly income after taxes, retirement, and savings
            income.Yearly.income_after_taxes = (income.Yearly.income * (1 - income.Federal_income_tax)) * (1 - income.State_income_tax);
            income.Yearly.income_after_retirement_and_taxes = ((income.Yearly.income - income.Yearly.retirement) * (1 - income.Federal_income_tax)) * (1 - income.State_income_tax);
            income.Yearly.income_after_retirement_savings_and_taxes = ((income.Yearly.income - income.Yearly.retirement) * (1 - income.Federal_income_tax)) * (1 - income.State_income_tax) - income.Yearly.savings;

            //calc monthly income
            income.Monthly.income = income.Yearly.income / 12;
            income.Monthly.retirement = income.Yearly.retirement / 12;
            income.Monthly.savings = income.Yearly.savings / 12;
            income.Monthly.income_after_taxes = income.Yearly.income_after_taxes / 12;
            income.Monthly.income_after_retirement_and_taxes = income.Yearly.income_after_retirement_and_taxes / 12;
            income.Monthly.income_after_retirement_savings_and_taxes = income.Yearly.income_after_retirement_savings_and_taxes / 12;

            return income;
        }

        /// <summary>
        /// Calculates income based on a change in the monthly information for the income.
        /// </summary>
        /// <param name="income"></param>
        /// <returns>the new income</returns>
        public static Income calcIncomeFromMonthly(Income income)
        {
            //calc monthly income after taxes, retirement, and savings
            income.Monthly.income_after_taxes = (income.Monthly.income * (1 - income.Federal_income_tax)) * (1 - income.State_income_tax);
            income.Monthly.income_after_retirement_and_taxes = ((income.Monthly.income - income.Monthly.retirement) * (1 - income.Federal_income_tax)) * (1 - income.State_income_tax);
            income.Monthly.income_after_retirement_savings_and_taxes = ((income.Monthly.income - income.Monthly.retirement) * (1 - income.Federal_income_tax)) * (1 - income.State_income_tax) - income.Monthly.savings;

            //calc yearly income
            income.Yearly.income = income.Monthly.income * 12;
            income.Yearly.retirement = income.Monthly.retirement * 12;
            income.Yearly.savings = income.Monthly.savings * 12;
            income.Yearly.income_after_taxes = income.Monthly.income_after_taxes * 12;
            income.Yearly.income_after_retirement_and_taxes = income.Monthly.income_after_retirement_and_taxes * 12;
            income.Yearly.income_after_retirement_savings_and_taxes = income.Monthly.income_after_retirement_savings_and_taxes * 12;

            return income;
        }

        /// <summary>
        /// Calculates the yearly expense from the monthly expense the user inputted.
        /// </summary>
        /// <param name="expense"></param>
        /// <returns>The new Expense with the yearly object filled out</returns>
        public static Expense calcYearlyExpense(Expense expense)
        {
            //calc housing
            expense.Yearly.housing.rent = expense.Monthly.housing.rent * 12;
            expense.Yearly.housing.utilities = expense.Monthly.housing.utilities * 12;

            //calc insurance
            expense.Yearly.insurance.car = expense.Monthly.insurance.car * 12;
            expense.Yearly.insurance.health = expense.Monthly.insurance.health * 12;
            expense.Yearly.insurance.dental = expense.Monthly.insurance.dental * 12;
            expense.Yearly.insurance.vision = expense.Monthly.insurance.vision * 12;
            expense.Yearly.insurance.life = expense.Monthly.insurance.life * 12;

            //calc loans
            expense.Yearly.loans.college = expense.Monthly.loans.college * 12;

            //calc rest of attributes
            expense.Yearly.phonebill = expense.Monthly.phonebill * 12;
            expense.Yearly.groceries = expense.Monthly.groceries * 12;

            //calc total expense
            expense.Monthly.total_expense = expense.Monthly.housing.getTotalHousingExpense() + expense.Monthly.insurance.getTotalInsuranceExpense() + expense.Monthly.loans.getTotalLoansExpense()
                                            + expense.Monthly.phonebill + expense.Monthly.groceries;
            expense.Yearly.total_expense = expense.Monthly.total_expense * 12;

            return expense;
        }

        /// <summary>
        /// Calculated the monthly expense from the yearly expense the user inputted.
        /// </summary>
        /// <param name="expense"></param>
        /// <returns>The new Expense with the monthly object filled out.</returns>
        public static Expense calcMonthlyExpense(Expense expense)
        {
            //calc housing
            expense.Monthly.housing.rent = expense.Yearly.housing.rent / 12;
            expense.Monthly.housing.utilities = expense.Yearly.housing.utilities / 12;

            //calc insurance
            expense.Monthly.insurance.car = expense.Yearly.insurance.car / 12;
            expense.Monthly.insurance.health = expense.Yearly.insurance.health / 12;
            expense.Monthly.insurance.dental = expense.Yearly.insurance.dental / 12;
            expense.Monthly.insurance.vision = expense.Yearly.insurance.vision / 12;
            expense.Monthly.insurance.life = expense.Yearly.insurance.life / 12;

            //calc loans
            expense.Monthly.loans.college = expense.Yearly.loans.college / 12;

            //calc rest of attributes
            expense.Monthly.phonebill = expense.Yearly.phonebill / 12;
            expense.Monthly.groceries = expense.Yearly.groceries / 12;

            //calc total expense
            expense.Yearly.total_expense = expense.Yearly.housing.getTotalHousingExpense() + expense.Yearly.insurance.getTotalInsuranceExpense() + expense.Yearly.loans.getTotalLoansExpense()
                                            + expense.Yearly.phonebill + expense.Yearly.groceries;
            expense.Monthly.total_expense = expense.Yearly.total_expense / 12;

            return expense;
        }
    }
}
