﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Objects
{
    public class Insurance
    {
        public double car { get; set; }
        public double health { get; set; }
        public double dental { get; set; }
        public double vision { get; set; }
        public double life { get; set; }

        /// <summary>
        /// Gets the total insurance expense.
        /// </summary>
        /// <returns>Total insurance expense.</returns>
        public double getTotalInsuranceExpense()
        {
            return car + health + dental + vision + life;
        }

        /// <summary>
        /// Checks if numbers entered for insurance are >= 0,
        /// </summary>
        /// <returns>True if all numbers entered for insurances are >=0.</returns>
        public bool enteredValidInsurance()
        {
            if (car < 0 || health < 0 || dental < 0 || vision < 0 || life < 0)
            {
                return false;
            }

            return true;
        }

    }
}
