﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Objects
{
    public class Loans
    {
        public double college { get; set; }

        /// <summary>
        /// Gets the total loans expense.
        /// </summary>
        /// <returns>Total loan expense.</returns>
        public double getTotalLoansExpense()
        {
            return college;
        }

        /// <summary>
        /// Checks if numbers entered for loans are >= 0.
        /// </summary>
        /// <returns>True if all numbers entered for loans are >= 0.</returns>
        public bool enteredValidLoans()
        {
            if (college < 0)
            {
                return false;
            }

            return true;
        }
    }
}
