﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Objects
{
    public class ExpenseReport
    {
        public Housing housing { get; set; }
        public Insurance insurance { get; set; }
        public Loans loans { get; set; }
        public double phonebill { get; set; }
        public double groceries { get; set; }
        public double total_expense { get; set; }

        /// <summary>
        /// Checks if a valid expense report was entered, where all numbers are greater than 0.
        /// </summary>
        /// <returns>True if valid report.</returns>
        public bool enteredValidReport()
        {
            if (housing.enteredValidHousing() && insurance.enteredValidInsurance() && loans.enteredValidLoans() && phonebill >= 0 && groceries >= 0)
            {
                return true;
            }

            return false;
        }
    }
}
