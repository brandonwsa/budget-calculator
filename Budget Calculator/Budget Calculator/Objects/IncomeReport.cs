﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Objects
{
    public class IncomeReport
    {
        public double income { get; set; }
        public double retirement { get; set; }
        public double savings { get; set; }
        public double income_after_taxes { get; set; }
        public double income_after_retirement_and_taxes { get; set; }
        public double income_after_retirement_savings_and_taxes { get; set; }
    }
}
