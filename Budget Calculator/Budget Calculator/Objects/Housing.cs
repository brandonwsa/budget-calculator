﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budget_Calculator.Objects
{
    public class Housing
    {
        public double rent { get; set; }
        public double utilities { get; set; }

        /// <summary>
        /// Gets the total housing expense.
        /// </summary>
        /// <returns>total housing expense.</returns>
        public double getTotalHousingExpense()
        {
            return rent + utilities;
        }

        /// <summary>
        /// Check if user entered valid numbers >= 0 for their housing.
        /// </summary>
        /// <returns>True if entered valid numbers >= 0.</returns>
        public bool enteredValidHousing()
        {
            if (rent < 0 || utilities < 0)
            {
                return false;
            }

            return true;
        }
    }
}
